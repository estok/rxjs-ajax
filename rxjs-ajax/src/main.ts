import { Ajax } from './ajax';
import { User, Device, App } from './models';
import { Observable } from 'rxjs';
import { tap, map, switchMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';



const API_URL = 'https://rx-microadmin.herokuapp.com';

const request = new Ajax();


request.get(`${API_URL}/users`)
  .pipe(
    switchMap( (users:Array<User>): Observable<Array<User>> => {
      const devices = [].concat(users)
      .map((user:any) => {
        const deviceUrl = `${API_URL}/devices?user_id=${user.id}`;
        //console.log(device)
        return request.get(deviceUrl)
          .pipe(
            map((devices:Array<Device>):User => {
              //console.log(devices)
              return{
                ...user,
                devices
              }
            })
          )
      })
      //console.log(devices)
      return forkJoin(devices)
    }),
    switchMap( (userDevices: Array<User>): Observable<Array<User>> => {
      const userDevice = userDevices
      
      .map((user:any) => {
        
        const deviceApp = user.devices.map( (device:any) => {
          const appUrl = `${API_URL}/apps?host_id=${device.id}`;
          //console.log(appUrl)

          return request.get(appUrl)
            .pipe(
              map( (app:Array<App>): Device => {
                //console.log(app)
                const sum = app.reduce((accum, apps) => accum + apps.size, 0);
                //console.log(sum)
                const Percent = Math.round(sum / device.capacity * 100);
                const loadPercent = `${Percent} %`;
                //console.log(loadPercent)
                return {
                  ...device,
                  app,
                  loadPercent
                }
              })
            )
        });

        return forkJoin(deviceApp).pipe(
          map((devices) => {
            //console.log(devices)
            return {
              ...user,
              devices
            }
          })
        )

      })
      //console.log(userDevice)
      return forkJoin(userDevice)
    }),

    tap(users => console.log('Users', users))

  )
.subscribe()

