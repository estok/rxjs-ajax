import {Observable} from 'rxjs';


export class Ajax {
    constructor() {
  
    }
    createRequest(method: string, url: string) {
  
      const stream = Observable.create((observer: any) => {
        const xhr = new XMLHttpRequest();
  
        xhr.open(method, url);
        xhr.onload = () => {
          if (xhr.status >= 200 && xhr.status < 400) {
  
            const response = JSON.parse(xhr.responseText)
            //console.log(response);
            observer.next(response);
          }
          else {
            observer.error('Error');
          }
          observer.complete();
        }
        xhr.send();
  
      })

      return stream

    }
  
    get(url: string) {
      return this.createRequest('GET', url)
  
    }
  
  }

