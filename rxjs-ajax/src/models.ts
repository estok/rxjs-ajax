interface User {
    id: string;
    name: string;
    email: string;
    devices: Device[];
  }
  
  interface Device {
    id: string;
    user_id: number;
    description: string;
    ip: string;
    capacity: number;
    apps?: App[];
  }
  
  interface App {
    id: string;
    title: string;
    size: number;
    host_id: string;
  }

  export {User, Device, App};